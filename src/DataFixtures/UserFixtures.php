<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use App\Entity\User;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
  private $passwordEncoder;

  public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
         $this->passwordEncoder = $passwordEncoder;
    }

  public function load(ObjectManager $manager)
    {
      $roles = ['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SUPER_ADMIN', 'ROLE_USER'];

      for ($u=0; $u<=3; $u++) {

         $user = new User();
         $user->setName("nameUser-".$u);
         $user->setSurnames("surnameUser-".$u);
         $user->setEmail('mail'.$u.'@mail.com');
         $user->setRoles($roles[$u]);

         $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            $u
        ));
        $manager->persist($user);

      }
      $manager->flush();


    }

    public function getOrder()
    {
        return 1;
    }
}

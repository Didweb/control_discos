<?php

namespace App\Form;

use App\Entity\Item;
use App\Entity\Lote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;

class ItemType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $optionStatus = ['Mint (M)'=>'Mint (M)',
                        'Near Mint (NM or M-)'=>'Near Mint (NM or M-)',
                        'Very Good Plus (VG+)'=>'Very Good Plus (VG+)',
                        'Very Good (VG)'=>'Very Good (VG)',
                        'Good Plus (G+)'=>'Good Plus (G+)',
                        'Good (G)'=>'Good (G)',
                        'Fair (F)'=>'Fair (F)',
                        'Poor (P)'=>'Poor (P)'];

        $idlote = $options['lote'];
        $builder
            ->add('codeDgs', TextType::class, ['attr' => ['class' => 'form-control  ']])
            ->add('name', TextType::class, ['attr' => ['class' => 'form-control  '],
                                            'required' => false
                                          ])
            ->add('catalogCode', TextType::class, ['attr' => ['class' => 'form-control  '],
                                            'required' => false
                                          ])
            ->add('mediaStatus', ChoiceType::class, array(
                          'attr' =>['class'=> 'form-control'],
                          'choices' => $optionStatus,
                          'label' => 'Media Status'
                        ))
            ->add('caseStatus', ChoiceType::class, array(
                          'attr' =>['class'=> 'form-control'],
                          'choices' => $optionStatus,
                          'label' => 'Case Status'
                        ))
            ->add('lote', EntityType::class, [
                              'class'         => Lote::class,
                              'choice_label'  => 'id',
                              'required' => true,
                              'query_builder' => function (EntityRepository $l) use ($idlote) {
                                                 return $l->createQueryBuilder('l')
                                                    ->where('l.id = :idlote')
                                                    ->setParameter('idlote', $idlote)
                                                    ->orderBy('l.id', 'ASC');
                                              },
                              'attr' => ['class' => 'd-none']
                              ])


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'lote' => null
              ]);
    }
}

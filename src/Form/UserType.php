<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $builder->getData();

        $builder
            ->add('email', EmailType::class, [
                          'attr' => ['class' => 'form-control  ',
                          'placeholder' => 'E-mail'],
                          'required' => true  ]
                          )
            ->add('name', TextType::class, ['attr' => ['class' => 'form-control  ']])
            ->add('surnames', TextType::class, ['attr' => ['class' => 'form-control  ']])
            ->add('roles', ChoiceType::class, array(
                          'attr' =>['class'=> 'form-control'],
                          'choices' => array(
                              'Role User' => 'ROLE_USER',
                              'Role Admin' => 'ROLE_ADMIN',
                              'Role Super Admin' => 'ROLE_SUPER_ADMIN'
                          ),
                          'label' => 'Role',
                          'data'=> $user->getRoles()[0]
      ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'attr' =>['class'=> 'form-control'],
                'required' => false,
                'first_options'  => array('label' => 'Password', 'empty_data' => 'void'),
                'second_options' => array('label' => 'Repeat Password', 'empty_data' => 'void')
              ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\Lote;
use App\Entity\User;
use App\Entity\Origin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class LoteType extends AbstractType
{

    protected $security;

    public function __construct(UsageTrackingTokenStorage $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $dateCalcualte = new \DateTime();
        $currentUser = $this->security->getToken()->getUser();
        $builder
            ->add('ref', TextType::class, ['attr' => ['class' => 'form-control  '],
                                          'label' => 'Referencia interna'])
            ->add('origin', EntityType::class , [
                              'class'         => Origin::class,
                              'choice_label'  => 'name',
                              'required' => true,
                              'label' => 'Origen del Lote',
                              'data' => null,
                              'attr' => ['class' => 'form-control']
                              ])
            ->add('urlLote', TextType::class, ['attr' => ['class' => 'form-control '],
                                               'required' => false,
                                               'label' => 'Url del lote'])
            ->add('price', NumberType::class, ['attr' => ['class' => 'form-control  '],
                                              'label' => 'Precio del lote (incluido envio aprox.)'])
            ->add('priceByItemSuggest', NumberType::class, ['data' => 0])
            ->add('priceByLoteSuggest', NumberType::class, ['data' => 0])
            ->add('profitPerCent', NumberType::class, ['attr' => ['class' => 'd-none  '], 'data' => 0.0])
            ->add('dateCreate', DateTimeType::class, [
                                            'widget' => 'single_text',
                                            'html5' => false,
                                            'format' => 'dd-MM-yyyy',
                                            'required' => true])
            ->add('dateAuctionClosing', DateTimeType::class, [
                                            'widget' => 'single_text',
                                            'attr' => ['class' => 'form-control  js-datepicker'],
                                            'html5' => false,
                                            'label' => 'Fecha finalización subasta',
                                            'format' => 'dd-MM-yyyy',
                                            'required' => false])
            ->add('user', EntityType::class , [
                              'class'         => User::class,
                              'choice_label'  => 'name',
                              'required' => true,
                              'data' => $currentUser,
                              'attr' => ['class' => 'd-none']
                              ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lote::class,
            'security' => Security::class
        ]);
    }
}

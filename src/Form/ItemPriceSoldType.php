<?php

namespace App\Form;

use App\Entity\Item;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ItemPriceSoldType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $iditem = $options['iditem'];
        $builder
            ->add('priceSale', NumberType::class, ['attr' => ['class' => 'form-control  '] ])
            ->add('idItem', HiddenType::class, ['data'=> $iditem, 'mapped'=>false ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'iditem' => null
              ]);
    }
}

<?php

namespace App\Form;

use App\Entity\LoteOption;
use App\Entity\Lote;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\UsageTrackingTokenStorage;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Doctrine\ORM\EntityRepository;

class LoteOptionType extends AbstractType
{



    public function buildForm(FormBuilderInterface $builder, array $options)
    {

      $idlote = $options['lote'];
        $builder
            ->add('margin', NumberType::class, ['attr' => ['class' => 'form-control  ',
                                                'placeholder' => 'Inserta %'],
                                                'label' => 'Opción con   margen...'] )
            ->add('lote', EntityType::class, [
                                        'class' => Lote::class,
                                        'choice_label'  => 'id',
                                        'required' => true,
                                        'query_builder' => function (EntityRepository $l) use ($idlote) {
                                                           return $l->createQueryBuilder('l')
                                                              ->where('l.id = :idlote')
                                                              ->setParameter('idlote', $idlote)
                                                              ->orderBy('l.id', 'ASC');
                                                        },
                                        'attr' => ['class' => 'd-none']
                              ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LoteOption::class,
            'lote' => null
              ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Lote;
use App\Form\LoteType;
use App\Entity\LoteOption;
use App\Form\LoteOptionType;
use App\Entity\Item;
use App\Services\Helpers;
use App\Services\HelperSessions;
use App\Form\ItemType;
use App\Repository\LoteRepository;
use App\Repository\ItemRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Classes\DatasDgs;
use Symfony\Contracts\HttpClient\HttpClientInterface;


/**
 * @Route("/admin/lote")
 */
class LoteController extends AbstractController
{
    private $conditionStatus = ['Mint (M)'=>'Mint (M)',
                    'Near Mint (NM or M-)'=>'Near Mint (NM or M-)',
                    'Very Good Plus (VG+)'=>'Very Good Plus (VG+)',
                    'Very Good (VG)'=>'Very Good (VG)',
                    'Good Plus (G+)'=>'Good Plus (G+)',
                    'Good (G)'=>'Good (G)',
                    'Fair (F)'=>'Fair (F)',
                    'Poor (P)'=>'Poor (P)'];

    private $totalSuggestPriceItems;
    private $nitems;
    private $helpers;
    private $session;

    public function __construct(Helpers $helpers, HelperSessions $helperSessions) {
      $this->helpers = $helpers;
      $this->helperSessions = $helperSessions;
      $this->session = $this->helperSessions->getSesion();
    }

    /**
     * @Route("/{filterlotestatus}/{filterloteinteres}/{sortcolumn}/{sortdirection}/list-lotes", name="lote_index", methods={"GET"})
     */
    public function index(LoteRepository $loteRepository,
                                        $filterlotestatus = 'all',
                                        $filterloteinteres = 'all',
                                        $sortcolumn = 'priceByLoteSuggest',
                                        $sortdirection = 'DESC'): Response
    {

      $this->session->set('filterlotestatus', $filterlotestatus);
      $this->session->set('filterloteinteres', $filterloteinteres);
      $this->session->set('sortcolumn', $sortcolumn);
      $this->session->set('sortdirection', $sortdirection);

      $listLotes = $loteRepository->findAllWithFilter($this->session);

        return $this->render('lote/index.html.twig', [
            'lotes' => $listLotes,
        ]);
    }


      /**
       * @Route("/{id}/{place}/lote-change-status", name="lote_change_status", methods={"GET","POST"})
       */
      public function changeStatusLote(Request $request, LoteRepository $loteRepository, $id, $place): Response
      {
        $em   = $this->getDoctrine()->getManager();
        $lote = $loteRepository->findById((int)$id)[0];
        $loteStatusChange = $this->getParameter('status_lote_comprado');
        if ($lote->getStatus() ==  $this->getParameter('status_lote_comprado') ) {
            $loteStatusChange = $this->getParameter('status_lote_estudio');
          }


        $lote->setStatus($loteStatusChange);
        $em->persist($lote);
        $em->flush();
        if($place == 'edit') {
            return $this->redirectToRoute('lote_show', ['id' => $id]);
          } else {
            return $this->redirectToRoute('lote_index', ['filterlotestatus'  => $this->session->get('filterlotestatus'),
                                                         'filterloteinteres' => $this->session->get('filterloteinteres'),
                                                         'sortcolumn'        => $this->session->get('sortcolumn'),
                                                         'sortdirection'     => $this->session->get('sortdirection')
                                                        ]);
          }

      }

    /**
     * @Route("/new", name="lote_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $lote = new Lote();
        $form = $this->createForm(LoteType::class, $lote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $lote->setStatus($this->getParameter('status_lote_estudio'));
            $entityManager->persist($lote);
            $entityManager->flush();


            return $this->redirectToRoute('lote_show', ['id' => $lote->getID() ]);
        }

        return $this->render('lote/new.html.twig', [
            'lote' => $lote,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}", name="lote_show", methods={"GET"})
     */
    public function show(Request $request, Lote $lote): Response
    {
        $item = new Item();
        $loteOption =  new LoteOption();

        $formItem = $this->createForm(ItemType::class, $item, ['lote'=> $lote->getId()]);
        $formItem->handleRequest($request);
        $priceByItem = 0;
        if(count($lote->getItems()) > 0) {
          $priceByItem = $lote->getPrice() / count($lote->getItems());
        }


        $formLoteOption = $this->createForm(LoteOptionType::class, $loteOption, ['lote'=> $lote->getId()]);

        $datas = ['priceByItem' => $priceByItem];
        return $this->render('lote/show.html.twig', [
            'lote' => $lote,
            'datas' => $datas,
            'formItem' => $formItem->createView(),
            'formLoteOption' => $formLoteOption->createView()
        ]);
    }



    /**
     * @Route("/{id}/delete-opciones", name="delete_options", methods={"GET"})
     */
    public function deleteLoteOption(Request $request, LoteRepository $loteRepository,$id): Response
    {

      $em     = $this->getDoctrine()->getManager();
      $lote   = $loteRepository->findById((int)$id);

      foreach($lote[0]->getLoteOptions() as $option) {
          $em->remove($option);
          $em->flush();
      }
      return $this->redirectToRoute('lote_show', ['id' => $id]);
    }


    /**
     * @Route("/{id}/update-opciones", name="update_options", methods={"GET"})
     */
    public function updateLoteOption(Request $request, LoteRepository $loteRepository, $id): Response
    {

      $entityManager = $this->getDoctrine()->getManager();
      $lote   = $loteRepository->findById((int)$id);

      $this->helpers->searchSuggestDiscoGs($lote[0]);


      $this->helpers->calculateDatasLotesOption($lote[0]);
      $this->helpers->calculateDatasLote($lote[0]);


      return $this->redirectToRoute('lote_show', ['id' => $id]);
    }



    /**
     * @Route("/new-lote-option", name="lote_new_lote_option", methods={"GET","POST"})
     */
    public function newLoteOption(Request $request, LoteRepository $loteRepository): Response
    {
      $loteOption   = new LoteOption();
      $idLote = $request->request->get('lote_option')['lote'];
      $lote   = $loteRepository->findById((int)$idLote);
      $entityManager = $this->getDoctrine()->getManager();

      $this->helpers->searchSuggestDiscoGs($lote[0]);

      $form = $this->createForm(LoteOptionType::class, $loteOption, ['lote'=> $idLote]);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

          $margin = (float)$request->request->get('lote_option')['margin'];

          $resultPrices = $this->helpers->calculatePriceByItemInLoteOption((float)$lote[0]->getPrice(),
                                                           $margin,
                                                           (float)count($lote[0]->getItems()));

          $loteOption->setPriceByLote((float)$resultPrices['byLote']);
          $loteOption->setPriceByItem((float)$resultPrices['byItem']);

          $entityManager->persist($loteOption);
          $entityManager->flush();


          $this->helpers->calculateDatasLote($lote[0]);

          return $this->redirectToRoute('lote_show', ['id' => $idLote]);
      }

      return $this->redirectToRoute('lote_show', ['id' => $idLote]);
    }


    /**
     * @Route("/new-item", name="lote_new_item", methods={"GET","POST"})
     */
    public function newItem(Request $request, LoteRepository $loteRepository): Response
    {
      $item   = new Item();
      $idLote = $request->request->get('item')['lote'];
      $codeDgs = $request->request->get('item')['codeDgs'];
      $lote   = $loteRepository->findById((int)$idLote);


      $form   = $this->createForm(ItemType::class, $item, ['lote'=> $idLote]);
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

          $this->helpers->createDatasDiscogsItem($item, $codeDgs);
          $item->setSold(0);
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($item);
          $entityManager->flush();

          return $this->redirectToRoute('lote_show', ['id' => $idLote]);
      }
      return $this->redirectToRoute('lote_show', ['id' => $idLote]);

    }



    /**
     * @Route("/{id}/update-item", name="update_item", methods={"GET"})
     */
    public function updateItemOne(Request $request, ItemRepository $itemRepository, $id): Response
    {
      $item  = $itemRepository->findById((int)$id);
      $codeDgs = $item[0]->getCodeDgs();
      $idLote = $item[0]->getLote();
      $this->helpers->createDatasDiscogsItem($item[0], $codeDgs);

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($item[0]);
      $entityManager->flush();

      return $this->redirectToRoute('lote_show', ['id' => $idLote->getId()]);

    }


    /**
     * @Route("/{idlote}/update-items-by-idlote", name="update_items_by_idlote", methods={"GET"})
     */
    public function updateItemsByIdLote(Request $request, LoteRepository $loteRepository, $idlote): Response
    {
        $lote  = $loteRepository->findById((int)$idlote);
        $entityManager = $this->getDoctrine()->getManager();

        foreach($lote[0]->getItems() as $item) {
          $codeDgs = $item->getCodeDgs();
          $this->helpers->createDatasDiscogsItem($item, $codeDgs);
          $entityManager->persist($item);
        }

        $entityManager->flush();

        return $this->redirectToRoute('lote_show', ['id' => $idlote]);
    }


    /**
     * @Route("/{id}/edit", name="lote_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lote $lote): Response
    {
        $form = $this->createForm(LoteType::class, $lote);
        $form->handleRequest($request);
        $entityManager = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            $this->helpers->searchSuggestDiscoGs($lote);
            $this->helpers->calculateDatasLotesOption($lote);
            $this->helpers->calculateDatasLote($lote);

            return $this->redirectToRoute('lote_show', ['id' => $lote->getID() ]);
        }


        return $this->render('lote/edit.html.twig', [
            'lote' => $lote,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="lote_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lote $lote): Response
    {
        $nameLote = $lote->getRef();

        if ($this->isCsrfTokenValid('delete'.$lote->getId(), $request->request->get('_token'))) {
          if($lote->getStatus() == $this->getParameter('status_lote_estudio') ) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lote);
            $entityManager->flush();
            $this->addFlash('success', 'Lote: '.$nameLote.' eliminado con éxito.');
            return $this->redirectToRoute('lote_index', ['filterlotestatus'  => $this->session->get('filterlotestatus'),
                                                         'filterloteinteres' => $this->session->get('filterloteinteres'),
                                                         'sortcolumn'        => $this->session->get('sortcolumn'),
                                                         'sortdirection'     => $this->session->get('sortdirection')
                                                        ]);

          } else {
            $this->addFlash('danger', 'El Lote: '.$nameLote.' no puede ser elimnado, ya que sus items pertenecen al inventario,
                              para poder elimnarlo se ha de cambiar su estado a "Estudio".');
          }

        }

        return $this->redirectToRoute('lote_edit', ['id' => $lote->getId() ]);


    }

    /**
     * @Route("/delete-item/{id}", name="lote_item_delete", methods={"DELETE"})
     */
    public function deleteItem(Request $request, Item $item): Response
    {

        if ($this->isCsrfTokenValid('delete'.$item->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lote_show', ['id' => $request->request->get('idlote')]);
    }



}

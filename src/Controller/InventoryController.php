<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Lote;
use App\Entity\Item;
use App\Services\Helpers;
use App\Services\HelperSessions;
use App\Repository\LoteRepository;
use App\Repository\ItemRepository;
use App\Form\ItemPriceSoldType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
/**
 * @Route("/admin/inventory")
 */
class InventoryController extends AbstractController
{

    private $session;
    private $helpers;
    public function __construct(Helpers $helpers, HelperSessions $helperSessions) {
      $this->helpers = $helpers;
      $this->helperSessions = $helperSessions;
      $this->session = $this->helperSessions->getSesion();
    }

    /**
     * @Route("/{filter}/{filterlote}/list", name="inventory_list", methods={"GET"})
     */
    public function index(Request $request, LoteRepository $loteRepository, $filter = 'all', $filterlote = 0): Response
    {

        $this->session->set('filter', $filter);
        $this->session->set('filterlote', $filterlote);

        $inventory = $loteRepository->findAllInventory($this->getParameter('status_lote_comprado'), $this->session);
        $this->get('session')->set('filterinventory', $filter);
        return $this->render('inventory/index.html.twig', [
                                'inventory' => $inventory ]);
    }



      /**
       * @Route("/item-change-pricesale", name="item_change_pricesale", methods={"POST"})
       */
      public function changePriceSaleItem(Request $request, ItemRepository $ItemRepository): Response
      {

        $id         = $request->request->get("idItem");
        $place      = $request->request->get("place");
        $priceSale  = $request->request->get("priceSale");
        $em   = $this->getDoctrine()->getManager();

        $item = $ItemRepository->findById((int)$id)[0];

        $item->setPriceSale($priceSale);
        $em->persist($item);
        $em->flush();

        if($place == 'lote') {
            return $this->redirectToRoute('lote_show', ['id' => $item->getLote()->getId()]);
          }
        return $this->redirectToRoute('inventory_list', ['filter'=> $this->session->get('filter'), 'filterlote'=> $this->session->get('filterlote')]);
      }

    /**
     * @Route("/{id}/{place}/item-change-sold", name="item_change_sold", methods={"GET","POST"})
     */
    public function changeSoldItem(Request $request, ItemRepository $ItemRepository, $id, $place): Response
    {

      $statusName = ['No Vendido', 'Vendido'];
      $em   = $this->getDoctrine()->getManager();
      $item = $ItemRepository->findById((int)$id)[0];
      $itemSold = 0;
      if ($item->getSold() ==  0) {
          $itemSold = 1;
        }


      $item->setSold($itemSold);
      $em->persist($item);
      $em->flush();
      $this->addFlash('success', 'Item: '.$item->getName().' Cambia de esatdo: '.$statusName[$itemSold]);

      if($place == 'lote') {
          return $this->redirectToRoute('lote_show', ['id' => $item->getLote()->getId()]);
        }
      return $this->redirectToRoute('inventory_list', ['filter'=> $this->session->get('filter')]);
    }




    /**
     * @Route("/imported-inventory", name="imported_inventory", methods={"GET","POST"})
     */
    public function importInventaryByIdRelaese(LoteRepository $loteRepository): Response
     {

      $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
      $datas = $serializer->decode(file_get_contents(__DIR__.'/../../public/assets/imported/data_test.csv'),
                                                    'csv',
                                                    array(CsvEncoder::DELIMITER_KEY => ','));


      $entityManager = $this->getDoctrine()->getManager();

      $n = 0;
      foreach($datas as $value) {
        $item = new Item();

        $item->setSold(0);
        $lote =  $loteRepository->findById((int)$value['idlote'])[0];
        $item->setLote($lote);
        $item->setMediaStatus($value['statusMedia']);
        $item->setCaseStatus($value['statuscover']);
        $item->setPriceSale((float)$value['price']);
        $this->helpers->createDatasDiscogsItem($item, $value['idrelease']);
        $entityManager->persist($item);
        $entityManager->flush();

        if($n == 5) {
          sleep(3);
          $n = 0;
        }
        $n++;
      }

      $this->addFlash('success', 'Imprtación terminada');
      return $this->render('inventory/imported.html.twig');

    }



}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\LoteRepository;
use App\Repository\ItemRepository;
use App\Services\Helpers;

class HomeController extends AbstractController
{


  private $helpers;

  public function __construct(Helpers $helpers) {
    $this->helpers = $helpers;
  }

    /**
     * @Route("/admin/home", name="home")
     */
    public function index(LoteRepository $loteRepository, ItemRepository $ItemRepository): Response
    {

      $totalCostsLotes = $loteRepository->findTotalPrice();
      $totalSale = $ItemRepository->findTotalSale();
      $lotesComprados = $loteRepository->findLotesComprados();
      $totalDatas = $ItemRepository->findTotalDatas($lotesComprados);
    //  $profit = ($totalSale[0]['total'] - $totalCostsLotes[0]['total']);
      $comisonApply = $this->helpers->calculateMinusComision($totalSale[0]['total']);
      return $this->render('home/index.html.twig', [
            'totalCostsLotes' => $totalCostsLotes[0]['total'],
            'totalSale' => $totalSale[0]['total'],
            'comisonApply' => $comisonApply,
            'totalDatas' => $totalDatas

        ]);
    }

    /**
     * @Route("/a", name="entry")
     */
    public function entry(): Response
    {
      $response = $this->forward('App\Controller\SecurityController::login');
      return $response;
    }
}

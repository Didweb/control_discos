<?php

namespace App\Repository;

use App\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function findTotalSale() {
      return $this->createQueryBuilder('i')
          ->select('SUM(i.PriceSale) as total')
          ->andWhere('i.sold = :val')
          ->setParameter('val', 1)
          ->orderBy('i.id', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }

    public function findTotalDatas($lotesComprados) {
      return $this->createQueryBuilder('i')
          ->select('AVG(i.PriceSale)  as mediaPrecioUnidad',
                    'SUM(i.PriceSale)  as sumaPrecios',
                    'COUNT(i.id)  as totalItems'
                    )
          ->where('i.sold = :val AND i.lote IN (:val2) ')
          ->setParameter('val', 0)
          ->setParameter('val2', $lotesComprados)
          ->getQuery()
          ->getResult()
      ;
    }


}

<?php

namespace App\Repository;

use App\Entity\Lote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lote[]    findAll()
 * @method Lote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lote::class);
    }

     /**
      * @return Lote[] Returns an array of Lote objects
      */
    public function findAllInventory($value, $allFilter)
    {
      $filter = $allFilter->get('filter');
      $filterLote = (int)$allFilter->get('filterlote');

        $filters = ['all' => '',
                    'sold' => '1',
                    'nosold' => '0'];

      if($filter == 'all') {
         $query = $this->createQueryBuilder('l')
            ->andWhere('l.status = :status')
            ->setParameter('status', $value);

          if($filterLote > 0) {
            $query->andWhere('l.id = :idlote')
                  ->setParameter('idlote', $filterLote);
          }

        return $query->orderBy('l.id', 'ASC')
                      ->getQuery()
                      ->getResult();
      } else {

        $query = $this->createQueryBuilder('l')
            ->select('l', 'i')
            ->innerJoin('l.items', 'i')
            ->where('l.status = :status  AND i.sold = :filter');

            if($filterLote > 0) {
              $query->andWhere('l.id = :idlote')
                    ->setParameter('idlote', $filterLote);
            }

            $query->setParameter('status', $value)
                  ->setParameter('filter', $filters[$filter]);

        return $query->orderBy('l.id', 'DESC')
                      ->getQuery()
                      ->getResult();
      }

    }



    public function findAllWithFilter($allFilter)
    {
        $filterlotestatus = $allFilter->get('filterlotestatus');
        $filterloteinteres = $allFilter->get('filterloteinteres');
        $sortcolumn = $allFilter->get('sortcolumn');
        $sortdirection = $allFilter->get('sortdirection');

        $query = $this->createQueryBuilder('l');

        if( $filterlotestatus != 'all') {
          $query->select('l');
          $query->where('l.status = :status')
                ->setParameter('status', $filterlotestatus);
        }

        if( $filterloteinteres != 'all') {
          $query->select('l', 'i')
                ->innerJoin('l.items', 'i');

          if($filterloteinteres == 'interes') {
            $query->andWhere('i.sold = :filterinteres')
                  ->andWhere('l.competency < 10 AND i.peopleWantIt = 1')
                  ->setParameter('filterinteres', $filterlotestatus);
          }

          if($filterloteinteres == 'nointeres') {
            $query->andWhere(' i.sold = :filterinteres')
                  ->andWhere('l.competency > 10 AND i.peopleWantIt = 0')
                  ->setParameter('filterinteres', $filterlotestatus);
          }
        }

       return $query->orderBy('l.'.$sortcolumn, $sortdirection)
                     ->getQuery()
                     ->getResult();
    }

  public function findLotesComprados() {
    return $this->createQueryBuilder('l')
        ->select('l.id')
        ->andWhere('l.status = :val')
        ->setParameter('val', 'Comprado')
        ->getQuery()
        ->getResult()
    ;
  }


    public function findTotalPrice() {
      return $this->createQueryBuilder('l')
          ->select('SUM(l.price) as total')
          ->andWhere('l.status = :val')
          ->setParameter('val', 'Comprado')
          ->orderBy('l.id', 'ASC')
          ->getQuery()
          ->getResult()
      ;
    }
}

<?php

namespace App\Entity;

use App\Repository\LoteOptionRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LoteOptionRepository::class)
 */
class LoteOption
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $margin;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $priceByItem;


    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $priceByLote;


    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;


    /**
     * @ORM\ManyToOne(targetEntity="Lote", inversedBy="loteOptions")
     **/
    private $lote;

    public function __construct() {

      $this->setDateCreate( new \DateTime("Now") );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMargin(): ?int
    {
        return $this->margin;
    }

    public function setMargin(int $margin): self
    {
        $this->margin = $margin;

        return $this;
    }

    public function getPriceByItem(): ?string
    {
        return $this->priceByItem;
    }

    public function setPriceByItem(string $priceByItem): self
    {
        $this->priceByItem = $priceByItem;

        return $this;
    }

    public function getPriceByLote(): ?string
    {
        return $this->priceByLote;
    }

    public function setPriceByLote(string $priceByLote): self
    {
        $this->priceByLote = $priceByLote;

        return $this;
    }

    public function getLote(): ?Lote
    {
        return $this->lote;
    }

    public function setLote(?Lote $lote): self
    {
        $this->lote = $lote;

        return $this;
    }

    public function getDateCreate(): ?\DateTimeInterface
    {
        return $this->dateCreate;
    }

    public function setDateCreate(\DateTimeInterface $dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }


}

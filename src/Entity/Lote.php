<?php

namespace App\Entity;

use App\Repository\LoteRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=LoteRepository::class)
 */
class Lote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $ref;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlLote;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $price;

    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;

    /**
     * @ORM\Column(type="string", length=50, options={"default": "Estudio"})
     */
    private $status;


    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $priceByItemSuggest;


    /**
     * @ORM\Column(type="decimal", precision=7, scale=2)
     */
    private $priceByLoteSuggest;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2, options={"default": 0.0}, nullable=true )
     */
    private $profitPerCent;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2, options={"default": 0.0}, nullable=true )
     */
    private $profitMoney;

    /**
     * @ORM\Column(type="decimal", precision=7, scale=2, options={"default": 0.0}, nullable=true )
     */
    private $profitMoneyAppliedComision;

    /**
     * @ORM\Column(type="integer", options={"default": 0.0}, nullable=true )
     */
    private $competency;

    /**
     * @var \DateTime
     * @Assert\DateTime()
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateAuctionClosing;

    /**
    * @ORM\OneToMany(targetEntity="LoteOption", mappedBy="lote", cascade={"persist", "remove"})
    * @ORM\OrderBy({"margin" = "ASC"})
    **/
    private $loteOptions;

     /**
     * @ORM\OneToMany(targetEntity="Item", mappedBy="lote", cascade={"persist", "remove"})
     **/
    private $items;

    /**
     * @ORM\ManyToOne(targetEntity="Origin", inversedBy="lotes")
     **/
    private $origin;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="lotes")
     **/
    private $user;

    public function __construct() {

      $this->setDateCreate( new \DateTime("Now") );
      $this->loteOptions = new ArrayCollection();
      $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }



    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getDateCreate(): ?\DateTime
    {
        return $this->dateCreate;
    }

    public function setDateCreate(\DateTime $dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }



    /**
     * @return Collection|LoteOption[]
     */
    public function getLoteOptions(): Collection
    {
        return $this->loteOptions;
    }

    public function addLoteOption(LoteOption $loteOption): self
    {
        if (!$this->loteOptions->contains($loteOption)) {
            $this->loteOptions[] = $loteOption;
            $loteOption->setLote($this);
        }

        return $this;
    }

    public function removeLoteOption(LoteOption $loteOption): self
    {
        if ($this->loteOptions->removeElement($loteOption)) {
            // set the owning side to null (unless already changed)
            if ($loteOption->getLote() === $this) {
                $loteOption->setLote(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setLote($this);
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->removeElement($item)) {
            // set the owning side to null (unless already changed)
            if ($item->getLote() === $this) {
                $item->setLote(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getOrigin(): ?Origin
    {
        return $this->origin;
    }

    public function setOrigin(?Origin $origin): self
    {
        $this->origin = $origin;

        return $this;
    }

    public function getUrlLote(): ?string
    {
        return $this->urlLote;
    }

    public function setUrlLote(string $urlLote): self
    {
        $this->urlLote = $urlLote;

        return $this;
    }

    public function getDateAuctionClosing(): ?\DateTimeInterface
    {
        return $this->dateAuctionClosing;
    }

    public function setDateAuctionClosing(?\DateTimeInterface $dateAuctionClosing): self
    {
        $this->dateAuctionClosing = $dateAuctionClosing;

        return $this;
    }

    public function getPriceByItemSuggest(): ?string
    {
        return $this->priceByItemSuggest;
    }

    public function setPriceByItemSuggest(string $priceByItemSuggest): self
    {
        $this->priceByItemSuggest = $priceByItemSuggest;

        return $this;
    }

    public function getPriceByLoteSuggest(): ?string
    {
        return $this->priceByLoteSuggest;
    }

    public function setPriceByLoteSuggest(string $priceByLoteSuggest): self
    {
        $this->priceByLoteSuggest = $priceByLoteSuggest;

        return $this;
    }

    public function getProfitPerCent(): ?string
    {
        return $this->profitPerCent;
    }

    public function setProfitPerCent(string $profitPerCent): self
    {
        $this->profitPerCent = $profitPerCent;

        return $this;
    }

    public function getProfitMoney(): ?string
    {
        return $this->profitMoney;
    }

    public function setProfitMoney(?string $profitMoney): self
    {
        $this->profitMoney = $profitMoney;

        return $this;
    }

    public function getProfitMoneyAppliedComision(): ?string
    {
        return $this->profitMoneyAppliedComision;
    }

    public function setProfitMoneyAppliedComision(?string $profitMoneyAppliedComision): self
    {
        $this->profitMoneyAppliedComision = $profitMoneyAppliedComision;

        return $this;
    }

    public function getCompetency(): ?int
    {
        return $this->competency;
    }

    public function setCompetency(?int $competency): self
    {
        $this->competency = $competency;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }


}

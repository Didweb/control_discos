<?php

namespace App\Entity;

use App\Repository\ItemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ItemRepository::class)
 */
class Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $codeDgs;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     */
    private $urlDiscoGs;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $mediaStatus;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $caseStatus;

    /**
     * @ORM\Column(type="integer", nullable=true )
     */
    private $numForSale;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": 0.0} )
     */
    private $peopleWantIt;



    /**
     * @ORM\Column(type="string", length=100)
     */
    private $catalogCode;

    /**
     * @ORM\Column(type="string", length=180, nullable=true )
     */
    private $recommendPriceDgs;

    /**
     * @ORM\Column(type="string", length=180, nullable=true )
     */
    private $PriceSale;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": 0} )
     */
    private $sold;

    /**
     * @ORM\ManyToOne(targetEntity="Lote", inversedBy="items")
     **/
    private $lote;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeDgs(): ?string
    {
        return $this->codeDgs;
    }

    public function setCodeDgs(string $codeDgs): self
    {
        $this->codeDgs = $codeDgs;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMediaStatus(): ?string
    {
        return $this->mediaStatus;
    }

    public function setMediaStatus(string $mediaStatus): self
    {
        $this->mediaStatus = $mediaStatus;

        return $this;
    }

    public function getCaseStatus(): ?string
    {
        return $this->caseStatus;
    }

    public function setCaseStatus(string $caseStatus): self
    {
        $this->caseStatus = $caseStatus;

        return $this;
    }

    public function getCatalogCode(): ?string
    {
        return $this->catalogCode;
    }

    public function setCatalogCode(string $catalogCode): self
    {
        $this->catalogCode = $catalogCode;

        return $this;
    }

    public function getRecommendPriceDgs(): ?string
    {
        return $this->recommendPriceDgs;
    }

    public function setRecommendPriceDgs(string $recommendPriceDgs): self
    {
        $this->recommendPriceDgs = $recommendPriceDgs;

        return $this;
    }

    public function getLote(): ?Lote
    {
        return $this->lote;
    }

    public function setLote(?Lote $lote): self
    {
        $this->lote = $lote;

        return $this;
    }

    public function getNumForSale(): ?int
    {
        return $this->numForSale;
    }

    public function setNumForSale(int $numForSale): self
    {
        $this->numForSale = $numForSale;

        return $this;
    }

    public function getPeopleWantIt(): ?bool
    {
        return $this->peopleWantIt;
    }

    public function setPeopleWantIt(bool $peopleWantIt): self
    {
        $this->peopleWantIt = $peopleWantIt;

        return $this;
    }

    public function getUrlDiscoGs(): ?string
    {
        return $this->urlDiscoGs;
    }

    public function setUrlDiscoGs(string $urlDiscoGs): self
    {
        $this->urlDiscoGs = $urlDiscoGs;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getPriceSale(): ?string
    {
        return $this->PriceSale;
    }

    public function setPriceSale(?string $PriceSale): self
    {
        $this->PriceSale = $PriceSale;

        return $this;
    }

    public function getSold(): ?bool
    {
        return $this->sold;
    }

    public function setSold(bool $sold): self
    {
        $this->sold = $sold;

        return $this;
    }
}

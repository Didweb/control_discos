<?php

namespace App\Entity;

use App\Repository\OriginRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=OriginRepository::class)
 */
class Origin
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Lote", mappedBy="origin", cascade={"persist", "remove"})
     **/
    private $lotes;

    public function __construct() {

      $this->lotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLotes(): ?Lote
    {
        return $this->lotes;
    }

    public function setLotes(?Lote $lotes): self
    {
        $this->lotes = $lotes;

        return $this;
    }

    public function addLote(Lote $lote): self
    {
        if (!$this->lotes->contains($lote)) {
            $this->lotes[] = $lote;
            $lote->setOrigin($this);
        }

        return $this;
    }

    public function removeLote(Lote $lote): self
    {
        if ($this->lotes->removeElement($lote)) {
            // set the owning side to null (unless already changed)
            if ($lote->getOrigin() === $this) {
                $lote->setOrigin(null);
            }
        }

        return $this;
    }
}

<?php
namespace App\Classes;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class DatasDgs
{
  private $httpClient;
  private $rootPath;
  private $auth;

  public function __construct(HttpClientInterface $httpClient)
  {
        $this->httpClient = $httpClient;
        $this->rootPath = 'https://api.discogs.com/';
        $this->auth = '&key=qenKXexWEaquGQIrdJYg&secret=QcdDPJnxGacROXfrFWYwRgwSiqKXmuXC';
  }


  public function dataReleasesById($releaseId) {

    $url = $this->rootPath.'releases/'.$releaseId;
    try {
      $response = $this->httpClient->request(
                  'GET',
                  $url
              );
        $statusCode = $response->getStatusCode();
        $content = null;
        if($statusCode == '200') {
          $content = $response->getContent();
          $content = $response->toArray();
        }
        return ['status'=>$statusCode,'content'=>$content];

    } catch (TransportExceptionInterface $e) {
        throw $e;
    }
  }


  public function dataPriceSuggestions(int $releaseId): Array
  {

    $url = $this->rootPath.'marketplace/price_suggestions/'.$releaseId.'?token=fKHuAVkxqfFQjYaiIjKQcnsVxmAumlsCAkiTtPmY';
    try {
        $response = $this->httpClient->request('GET',$url);
        $statusCode = $response->getStatusCode();
        $content = [$url];
         if($statusCode == '200') {
          $content = $response->getContent();
          $content = $response->toArray();
         }

        return ['status'=>$statusCode,'content'=>$content];

    } catch (TransportExceptionInterface $e) {
        throw $e;
    }

  }

}

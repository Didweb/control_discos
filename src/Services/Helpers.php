<?php
namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Classes\DatasDgs;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Helpers
{

  private $em;
  private $httpClient;

  public function __construct(EntityManagerInterface $em, HttpClientInterface $httpClient, ParameterBagInterface $params) {
    $this->em = $em;
    $this->httpClient = $httpClient;
    $this->comision = $params->get('comision_discogs');
  }


  public function searchSuggestDiscoGs($lote) {
    $this->totalSuggestPriceItems = 0.0;
    $this->nitems = 0;
    $dataPriceSuggest = new DatasDgs($this->httpClient);

    foreach ($lote->getItems() as $item) {
      $codeDgs = $item->getCodeDgs();
      $suggestPrices = $dataPriceSuggest->dataPriceSuggestions($codeDgs);

      foreach($suggestPrices['content'] as $code => $price) {
        if($code == $item->getMediaStatus()) {
          if(isset($price['value'])) {
            $item->setRecommendPriceDgs($price['value']);
            $this->totalSuggestPriceItems = $this->totalSuggestPriceItems + (float)$price['value'];
          }

          $this->nitems++;
        }
      }
      $this->em->persist($item);
      $this->em->flush();
    }

    if(count($lote->getItems()) > 0) {
      $lote->setPriceByItemSuggest((float)$this->totalSuggestPriceItems / (float)count($lote->getItems()));
      $lote->setPriceByLoteSuggest((float)$this->totalSuggestPriceItems);
    }

    $this->em->persist($lote);
    $this->em->flush();

  }


  public function calculateDatasLotesOption($lote) {

    foreach($lote->getLoteOptions() as $loteOption) {

      $resultPrices = $this->calculatePriceByItemInLoteOption($lote->getPrice(),
                                                             $loteOption->getMargin(),
                                                             (float)count($lote->getItems()));

      $loteOption->setPriceByLote((float)$resultPrices['byLote']);
      $loteOption->setPriceByItem((float)$resultPrices['byItem']);
      $loteOption->setDateCreate( new \DateTime('now'));

      $this->em->persist($loteOption);
      $this->em->flush();

    }
  }


  public function calculatePriceByItemInLoteOption($lotePrice, $margin, $totalItems) {
    $priceByLote = $lotePrice + (($lotePrice*$margin)/100);
    $priceByItem = (float)$priceByLote / $totalItems;
    return ['byItem'=>$priceByItem, 'byLote' => $priceByLote];
  }

  public function calculateDatasLote($lote) {

    $precioCoste = $lote->getPrice();
    $precioVenta = $lote->getPriceByLoteSuggest();

    $profit = $this->calucalteProfit($precioVenta, $precioCoste);
    $lote->setProfitPerCent($profit);

    $profitMoney = $precioVenta - $precioCoste;
    $lote->setProfitMoney($profitMoney);
    $profitMinusComision = $this->calculateMinusComision($profitMoney);
    $lote->setProfitMoneyAppliedComision($profitMinusComision);

    $totalCom = 0;
    foreach ($lote->getItems() as $item) {
      $totalCom = $totalCom + $item->getNumForSale();
    }

    $mediaCom = 0;
    if (count($lote->getItems()) > 0) {
      $mediaCom = $totalCom / count($lote->getItems());

    }
    $lote->setCompetency($mediaCom);


    $this->em->persist($lote);
    $this->em->flush();

  }

  public function calculateMinusComision($profitMoney) {
    $money = $profitMoney / (1 +  ($this->comision / 100));
    return $money;
  }

  private function calucalteProfit($precioVenta, $precioCoste) {
    $profit = 0;
    $ganancia = $precioVenta - $precioCoste;
    if($ganancia > 0 || $precioVenta > 0) {
      $profit = ($ganancia * 100) / $precioVenta;
    }

    return $profit;
  }


  public function createDatasDiscogsItem($item, $codeDgs) {

    $dataDgs = new DatasDgs($this->httpClient);
    $releaseItem = $dataDgs->dataReleasesById($codeDgs);
    $dataPricesSuggestion = $dataDgs->dataPriceSuggestions($codeDgs);
// dump($releaseItem['content']);
    if($releaseItem['content']) {

      $artistes = '';
      foreach ($releaseItem['content']['artists'] as $artist) {
        $artistes .= $artist['name'].' - ';
        }
      $name = $artistes.' '.$releaseItem['content']['title'];

      $codes = '';
      foreach ($releaseItem['content']['labels'] as $code) {
        $codes .= $code['name'].' '.$code['catno'].' ';
        }
      $catalogCode = $codes;

      $numForSale =  $releaseItem['content']['num_for_sale'];

      $peopleWantIt = 0;
      $have = $releaseItem['content']['community']['have'];
      $want = $releaseItem['content']['community']['want'];
      $balance = $have - $want;
      if($balance <= 0) {
        $peopleWantIt =  1;
        }
      $urlDiscogs = $releaseItem['content']['uri'];
      $year = $releaseItem['content']['year'];

      $priceSuggestion = '0.0';

      foreach($dataPricesSuggestion['content'] as $code => $price) {
        if($code == $item->getMediaStatus()) {
          if(isset($price['value'])) {
            $priceSuggestion = $price['value'];
            }
          }
        }

      $item->setCodeDgs($codeDgs);
      $item->setName($name);
      $item->setCatalogCode($catalogCode);
      $item->setRecommendPriceDgs($priceSuggestion);
      $item->setNumForSale($numForSale);
      $item->setPeopleWantIt($peopleWantIt);
      $item->setUrlDiscoGs($urlDiscogs);
      $item->setYear($year);

    } else {
      $item->setPeopleWantIt(0);
      $item->setNumForSale(0);
    }


  }
}

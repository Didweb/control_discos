<?php
namespace App\Services;


use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
//use Symfony\Component\HttpFoundation\Session\Session;

class HelperSessions
{

    private $sessions;
    private $request;

    public function __construct(SessionInterface $session, RequestStack $requestStack) {
      $this->session = $session;
      $this->request = $requestStack->getCurrentRequest();
      $this->initSessionListInventory();
    }

    public function initSessionListInventory() {

      $this->session = $this->request->getSession();

      if(!$this->request->getSession()->has('filter')) {
        $this->session->set('filter', 'all');
      }

      if(!$this->request->getSession()->has('filterlote')) {
        $this->session->set('filterlote', 0);
      }


      if(!$this->request->getSession()->has('filterlotestatus')) {
        $this->session->set('filterlotestatus', 'all');
      }

      if(!$this->request->getSession()->has('filterloteinteres')) {
        $this->session->set('filterloteinteres', 'all');
      }

      if(!$this->request->getSession()->has('sortcolumn')) {
        $this->session->set('sortcolumn', 'id');
      }

      if(!$this->request->getSession()->has('sortdirection')) {
        $this->session->set('sortdirection', 'DESC');
      }

    }

    public function  getSesion() {
      return $this->session;
    }
}

-- Doctrine Migration File Generated on 2020-12-06 11:01:55

-- Version DoctrineMigrations\Version20201206110152
ALTER TABLE lote ADD price_by_item_suggest NUMERIC(7, 2) NOT NULL, ADD price_by_lote_suggest NUMERIC(7, 2) NOT NULL;
ALTER TABLE lote_option DROP price_by_item_suggest, DROP price_by_lote_suggest;

-- Doctrine Migration File Generated on 2020-12-06 10:34:31

-- Version DoctrineMigrations\Version20201206103427
ALTER TABLE lote_option ADD price_by_item_suggest NUMERIC(7, 2) NOT NULL, ADD price_by_lote_suggest NUMERIC(7, 2) NOT NULL;
